
process.env.SECRET = 'banana';

const mock = require('mock-require');
const MockMap = require('./mock/Map');
mock('../lib/Map', MockMap);
const MockRover = require('./mock/Rover');
mock('../lib/Rover', MockRover);

const dir = require('../lib/constants').directions;
const ter = require('../lib/constants').terrains;
const cmd = require('../lib/constants').commands;
const app = require('../server').app;
const rover = require('../server').rover;

const chai = require('chai');
const chaiHttp = require('chai-http');
const should = chai.should();

chai.use(chaiHttp);

function sense() {
  return chai.request(app)
    .get('/sense')
    .set('Authorization', 'banana')
}

function exec(command) {
  return chai.request(app)
    .get('/exec')
    .set('Authorization', 'banana')
    .send({ command })
}

function execSeries(commands) {
  return chai.request(app)
    .get('/execSeries')
    .set('Authorization', 'banana')
    .send({ commands })
}

let commandSeries = [
  cmd.MOVE_FWD,
  cmd.TURN_CCW,
  cmd.MOVE_FWD,
  cmd.TURN_CW,
  cmd.MOVE_FWD,
  cmd.TURN_CCW,
  cmd.MOVE_BWD, // should stop here
  cmd.TURN_CCW,
  cmd.MOVE_BWD
];

// sensorData for positions traveled while executing 'commandSeries'
let senseData = [
  {
    [dir.NORTH]: [ter.FREE],
    [dir.SOUTH]: [ter.OUTSIDE, ter.OBSTACLE],
    [dir.EAST]:  [ter.OBSTACLE],
    [dir.WEST]: [ter.OBSTACLE]
  },
  {
    [dir.NORTH]: [ter.OBSTACLE],
    [dir.SOUTH]: [ter.FREE],
    [dir.EAST]:  [ter.FREE],
    [dir.WEST]: [ter.FREE]
  },
  {
    [dir.NORTH]: [ter.FREE],
    [dir.SOUTH]: [ter.OBSTACLE],
    [dir.EAST]:  [ter.FREE],
    [dir.WEST]: [ter.OUTSIDE, ter.FREE]
  },
  {
    [dir.NORTH]: [ter.OUTSIDE, ter.OBSTACLE],
    [dir.SOUTH]: [ter.FREE],
    [dir.EAST]:  [ter.OBSTACLE],
    [dir.WEST]: [ter.OUTSIDE, ter.FREE]
  }
];


describe('Authentication system', () => {
  it('fails to authenticate with wrong secret', (done) => {
    chai.request(app)
    .get('/sense')
    .set('Authorization', 'melone')
    .end( (err, res) => {
      res.should.have.status(401);
      res.body.should.be.an('object');
      res.body.should.have.property('message');
      res.body.message.should.be.eql('Thou shall not pass');
      done()
    })
  });

  it('authenticates correctly with right secret', (done) => {
    sense()
    .end( (err, res) => {
      res.should.have.status(200);
      done()
    })
  })
});

describe('Remote rover', () => {

  beforeEach(() => {
    rover.setPositioning({
      position: { x: 1, y: 0 },
      facing: dir.NORTH
    })
  });

  it('checks that request is properly formatted', (done) => {
    chai.request(app)
    .get('/exec')
    .set('Authorization', 'banana')
    .send({ why: 42 })
    .end( (err, res) => {
      res.should.have.status(400);

      exec('NOT A COMMAND')
      .end( (err, res) => {
        res.should.have.status(400);

        execSeries([ cmd.MOVE_FWD, 777, cmd.TURN_CW ])
        .end( (err, res) => {
          res.should.have.status(400);
          done()
        })
      })
    })
  });

  it('senses sorrounding terrain', (done) => {
    sense()
    .end( (err, res) => {
      res.should.have.status(200);
      res.body.data.should.be.eql(senseData[0]);
      done()
    })
  });

  it('is allowed to move into free terrain', (done) => {
    exec(cmd.MOVE_FWD)
    .end( (err, res) => {
      res.should.have.status(200);
      res.body.data.should.be.eql(senseData[1]);
      done()
    })
  });

  it('isn\'t allowed to move into obstacle', (done) => {
    exec(cmd.MOVE_BWD)
    .end( (err, res) => {
      res.should.have.status(200);
      res.body.should.have.property('error');
      res.body.error.should.be.eql('Unable to move');
      done()
    })
  });

  it('turns clockwise and senses surroundings', (done) => {
    exec(cmd.TURN_CW)
    .end( (err, res) => {
      res.should.have.status(200);
      res.body.data.should.be.eql(senseData[0]);
      done()
    })
  });

  it('receives multiple commands and returns array of sensor data', (done) => {
    execSeries(commandSeries.slice(0, 3))
    .end( (err, res) => {
      res.should.have.status(200);
      res.body.data.should.be.an('array').of.length(3);
      res.body.data[0].should.be.eql(senseData[1]);
      res.body.data[1].should.be.eql(senseData[1]);
      res.body.data[2].should.be.eql(senseData[2]);
      done()
    })
  });

  it('stops when unable to move and returns sensor data', (done) => {
    execSeries(commandSeries)
    .end( (err, res) => {
      res.should.have.status(200);
      res.body.data.should.be.an('array').of.length(6);
      res.body.data[5].should.be.eql(senseData[3]);
      res.body.should.have.property('error');
      res.body.error.should.be.eql('Unable to move');
      done()
    })
  })
})

describe('Map', () => {
  it('is vertically circular', (done) => {
    rover.setPositioning({
      position: { x: 3, y: 1 },
      facing: dir.NORTH
    });

    let commands = [];
    for (let i=0; i<8; i++) commands.push(cmd.MOVE_FWD);

    execSeries(commands)
    .end( (err, res) => {
      res.should.have.status(200);
      res.body.data.should.be.an('array').of.length(commands.length);
      res.body.should.not.have.property('error');
      done()
    })
  });

  it('is horizontally circular', (done) => {
    rover.setPositioning({
      position: { x: 3, y: 1 },
      facing: dir.EAST
    });

    let commands = [];
    for (let i=0; i<10; i++) commands.push(cmd.MOVE_BWD);

    execSeries(commands)
    .end( (err, res) => {
      res.should.have.status(200);
      res.body.data.should.be.an('array').of.length(commands.length);
      res.body.should.not.have.property('error');
      done()
    })
  })
})
