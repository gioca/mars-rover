const _ = require('lodash');
const randInt = require('random-int');

function Map(width, height) {
  this._width = width;
  this._height = height;
  this._obstacles = [];

  let obstaclesNumber = width * height * 0.1;
  for (let i=0; i<obstaclesNumber; i++)
    this._obstacles.push(this.randomFreePoint());
}

Map.prototype.width = function () {
  return this._width;
}

Map.prototype.height = function () {
  return this._height;
}

Map.prototype.hasObstacle = function (point) {
  let x = point.x,
      y = point.y;
  return !!_.find(this._obstacles, function(p) { return  p.x === x && p.y === y })
}

Map.prototype.randomPoint = function () {
  return {
    x: randInt(this._width-1),
    y: randInt(this._height-1)
  }
}

Map.prototype.randomFreePoint = function () {
  let point = this.randomPoint();
  while (this.hasObstacle(point))
    point = this.randomPoint();
  return point
}

Map.prototype.outOfBounds = function (point) {
  return point.x < 0 || point.x >= this._width ||
         point.y < 0 || point.y >= this._height
}

module.exports = Map

