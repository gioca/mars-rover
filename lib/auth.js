/*
 * Dead simple authentication system,
 * we don't want to let anybody control our rover
 */

const secret = process.env.SECRET || "123";

function checkToken(req, res, next) {

  if (req.get('Authorization') !== secret) {
    res.status(401);
    return res.send({ message: "Thou shall not pass" })
  }

  next();
}

module.exports = {
  checkToken
}
