const _ = require('lodash');

const Map = require('./Map');
const dir = require('./constants').directions;
const ter = require('./constants').terrains;

class Rover {
  constructor () {
    this.map = new Map(10, 6);
    this.pos = this.map.randomFreePoint();
    this.facing = dir.NORTH;
  }

  print () {
    console.log(this.map);
    console.log(this.map._obstacles.length);
  }

  getSensorData (cb) {
    let pos = this.pos;
    let res = {
      [dir.NORTH]: this.sense({ x: pos.x, y: pos.y+1}),
      [dir.SOUTH]: this.sense({ x: pos.x, y: pos.y-1}),
      [dir.EAST]:  this.sense({ x: pos.x+1, y: pos.y}),
      [dir.WEST]:  this.sense({ x: pos.x-1, y: pos.y})
    };

    cb(null, res);
  }

  moveForward (cb) {
    if (this._move(1))
      this.getSensorData(cb);
    else
      cb({ error: "Unable to move" });
  }

  moveBackwards (cb) {
    if (this._move(-1))
      this.getSensorData(cb);
    else
      cb({ error: "Unable to move" });
  }

  turnClockwise (cb) {
    this._turnCW();
    this.getSensorData(cb);
  }

  turnCounterClockwise (cb) {
    this._turnCW(); this._turnCW(); this._turnCW();
    this.getSensorData(cb);
  }

  _turnCW () {
    switch (this.facing) {
      case dir.NORTH:
        this.facing = dir.EAST; break;
      case dir.EAST:
        this.facing = dir.SOUTH; break;
      case dir.SOUTH:
        this.facing = dir.WEST; break;
      case dir.WEST:
        this.facing = dir.NORTH; break;
    }
  }

  _move (amount) {
    let newPos = this._newPosition(amount);

    if (_.includes(this.sense(newPos), ter.OBSTACLE))
      return false;

    this.pos = newPos;
    return true
  }

  _newPosition (amount) {
    let newPos = { x: this.pos.x, y: this.pos.y };

    switch (this.facing) {
      case dir.NORTH:
        newPos.y += amount; break;
      case dir.SOUTH:
        newPos.y -= amount; break;
      case dir.WEST:
        newPos.x -= amount; break;
      case dir.EAST:
        newPos.x += amount;
    }

    this._normalizePosition(newPos);

    return newPos
  }

  _normalizePosition (pos) {
    pos.x = mod(pos.x, this.map.width());
    pos.y = mod(pos.y, this.map.height());
  }

  sense (point) {
    let terrain = [];

    if (this.map.outOfBounds(point)) {
      this._normalizePosition(point);
      terrain.push(ter.OUTSIDE);
    }
    if (this.map.hasObstacle(point))
      terrain.push(ter.OBSTACLE);
    else
      terrain.push(ter.FREE);

    return terrain
  }
}

// becaus JS '%' operator doesn't handle negative numbers as desired
function mod(n, m) {
  return ((n % m) + m) % m;
}

module.exports = Rover

